import os
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

import scipy.stats
from scipy import interp

from sklearn.metrics import confusion_matrix
from sklearn.metrics import roc_curve, auc
import sklearn.preprocessing

from utils.dp import Node

sns.set()
sns.set_style("whitegrid")


def plot_rotated_image_preds(predictions, degrees, title=None, save_fig=None, close=False):
    """
    Args:
        rotated_images:
        rotated_targets:
        degrees:
        predictions: Probability output of neual network
    """

    pred_classes = np.unique(np.argmax(predictions, 1))
    print(pred_classes)
    relevant_preds = predictions[:, pred_classes]

    plt.figure(figsize=[6, 6])

    class_color = {0: 'tab:olive',
                   1: 'tab:pink',
                   2: 'tab:green',
                   3: 'tab:red',
                   4: 'tab:purple',
                   5: 'tab:brown',
                   6: 'tab:cyan',
                   7: 'tab:gray',
                   8: 'tab:blue',
                   9: 'tab:orange'}

    for i in range(pred_classes.shape[0]):
        plt.plot(degrees, relevant_preds[:, i], color=class_color[pred_classes[i]])

    if title is not None:
        plt.title(title)
    plt.legend(pred_classes, loc='center left')
    plt.xlim([0, degrees[-1]])
    plt.xlabel('Rotation Degree')
    plt.ylabel('Classification Probability')
    if save_fig is not None:
        plt.savefig(save_fig, bbox_inches="tight")
    if close is True:
        plt.close()


def confidence_hist_wrong_correct(predictions, targets=None, bins=10, color='#1f77b4', title=None, save_fig=None):
    """

    """
    unified_bins = np.arange(0., 1. + bins**-1, bins**-1)

    wrong_pred_probs = []
    correct_pred_probs = []

    for i, pred in enumerate(predictions):
        if np.argmax(pred) == targets[i]:
            correct_pred_probs.append(np.max(pred))
        else:
            wrong_pred_probs.append(np.max(pred))

    # Calculate bin weights to norm
    weights_cor = np.ones_like(correct_pred_probs) / float(len(correct_pred_probs))
    weights_wro = np.ones_like(wrong_pred_probs) / float(len(wrong_pred_probs))

    # Plot for correct predictions
    fig = plt.figure(figsize=(6, 4))
    fig.tight_layout()

    plt.hist(correct_pred_probs, bins=unified_bins, weights=weights_cor)
    plt.axvline(np.mean(correct_pred_probs), linestyle='dashed', linewidth=1)
    plt.xlabel("Confidence ($\\tau$)")
    plt.ylabel("% of Samples")
    plt.ylim(0.0, 1.0)
    plt.xlim(0.0, 1.0)
    plt.legend(["# correct preds: {}".format(len(correct_pred_probs))])

    if save_fig is not None:
        temp_path = save_fig.split(".png")
        plt.savefig(temp_path[0] + "_correct.png", bbox_inches="tight")

    if title is not None:
        plt.title(title + " - Correct Preds")

    plt.show()

    # Plot for wrong predictions
    fig = plt.figure(figsize=(6, 4))
    fig.tight_layout()

    plt.hist(wrong_pred_probs, bins=unified_bins, weights=weights_wro)
    plt.axvline(np.mean(wrong_pred_probs), linestyle='dashed', linewidth=1)
    plt.xlabel("Confidence ($\\tau$)")
    plt.ylabel("% of Samples")
    plt.ylim(0.0, 1.0)
    plt.xlim(0.0, 1.0)
    plt.legend(["# wrong preds: {}".format(len(wrong_pred_probs))])

    if save_fig is not None:
        temp_path = save_fig.split(".png")
        plt.savefig(temp_path[0] + "_wrong.png", bbox_inches="tight")

    if title is not None:
        plt.title(title + " - Wrong Preds")

    return wrong_pred_probs, correct_pred_probs


def confidence_curve_wrong_correct(outputs, targets=None, bins=10, color='#1f77b4', label=None, title=None, save_fig=None):
    """

    """
    unified_bins = np.arange(0., 1. + bins**-1, bins**-1)

    # Calculte correct/wrong
    wrong_pred_probs = []
    correct_pred_probs = []
    weights_cor = []
    weights_wro = []

    for j, output in enumerate(outputs):
        temp_wrong_pred_probs = []
        temp_correct_pred_probs = []
        temp_targets = targets[j]

        for i, pred in enumerate(output):
            if np.argmax(pred) == temp_targets[i]:
                temp_correct_pred_probs.append(np.max(pred))
            else:
                temp_wrong_pred_probs.append(np.max(pred))

        wrong_pred_probs.append(temp_wrong_pred_probs)
        correct_pred_probs.append(temp_correct_pred_probs)

        # Calculate bin weights to norm
        weights_cor.append(np.ones_like(correct_pred_probs) / float(len(correct_pred_probs)))
        weights_wro.append(np.ones_like(wrong_pred_probs) / float(len(wrong_pred_probs)))

    fig = plt.figure(figsize=(12, 4))
    ax = []

    ax.append(plt.subplot(1, 2, 1))
    for i, pred in enumerate(correct_pred_probs):
        sns.distplot(pred, bins=unified_binsunified_bins, hist=False, kde=True, kde_kws={'linewidth': 1, 'cumulative': True}, label=label[i])
    plt.xlabel("Confidence")
    plt.ylabel("% of Samples")

    ax.append(plt.subplot(1, 2, 2))
    for i, pred in enumerate(wrong_pred_probs):
        sns.distplot(pred, bins=unified_bins, hist=False, kde=True, kde_kws={'linewidth': 1, 'cumulative': True}, label=label[i])
    plt.xlabel("Confidence")
    plt.ylabel("% of Samples")
    #plt.xlim(-0.05, 1.05)

    for axis in ax:
        axis.label_outer()
    fig.tight_layout()

    if save_fig is not None:
        plt.savefig(save_fig, bbox_inches="tight")


def cumulative_confidence_curve_wrong_correct(outputs, targets, bins=100, labels=None, save_fig=None, title=None):
    """
    """

    # Calculte correct/wrong
    wrong_pred_probs = []
    correct_pred_probs = []
    weights_cor = []
    weights_wro = []

    for j, output in enumerate(outputs):
        temp_wrong_pred_probs = []
        temp_correct_pred_probs = []
        temp_targets = targets[j]

        for i, pred in enumerate(output):
            if np.argmax(pred) == temp_targets[i]:
                temp_correct_pred_probs.append(np.max(pred))
            else:
                temp_wrong_pred_probs.append(np.max(pred))

        wrong_pred_probs.append(temp_wrong_pred_probs)
        correct_pred_probs.append(temp_correct_pred_probs)

        # Calculate bin weights to norm
        weights_cor.append(np.ones_like(correct_pred_probs) / float(len(correct_pred_probs)))
        weights_wro.append(np.ones_like(wrong_pred_probs) / float(len(wrong_pred_probs)))

    unified_bins = np.arange(0., 1. + bins**-1, bins**-1)

    fig = plt.figure(figsize=(6, 4))
    fig.tight_layout()
    # # ax = []

    # ax.append(plt.subplot(1, 2, 1))
    for i, pred in enumerate(correct_pred_probs):
        num_examples, confidence = np.histogram(pred, bins=unified_bins)
        cumulative_examples = np.cumsum(num_examples)
        # Plot surival cumulative curve
        plt.plot(confidence[:-1], len(pred) - cumulative_examples)
    plt.xlabel("Confidence ($\\tau$)")
    plt.ylabel("# of Samples $p(y|x) \\geq \\tau$")
    if title is not None:
        plt.title(title[0])
    if labels is not None:
        plt.legend(labels, fontsize="small")

    if save_fig is not None:
        plt.savefig(save_fig + "_correct.png", bbox_inches="tight")
    plt.show()

    fig = plt.figure(figsize=(6, 4))
    fig.tight_layout()

    # ax.append(plt.subplot(1, 2, 2))
    for i, pred in enumerate(wrong_pred_probs):
        num_examples, confidence = np.histogram(pred, bins=unified_bins)
        cumulative_examples = np.cumsum(num_examples)
        # Plot surival cumulative curve
        plt.plot(confidence[:-1], len(pred) - cumulative_examples)
    plt.xlabel("Confidence ($\\tau$)")
    if title is not None:
        plt.title(title[0])
    # plt.ylabel("# of Samples $p(y|x) \\geq \\tau$")
    if labels is not None:
        plt.legend(labels, fontsize="small")

    # for axis in ax:
    #     axis.label_outer()

    if save_fig is not None:
        plt.savefig(save_fig + "_wrong.png", bbox_inches="tight")


def confidence_hist(predictions, targets=None, bins=10, color='#1f77b4', title=None, save_fig=None):
    """ Plots a histogram of the prediction distribution over the confidence.

    In addition the accuracy and mean confidence will be plotted in the
    histogram.

    Args:
    predictions: numpy array
        predictions of a classifier
    targets: numpy array
        ground truth labels as positional arg
    bins: int - default: 10
        number of bins in the histogram
    title: string - default: None
        title of the the plot
    save_fig: string - default: None
        path where to save the plot
    """

    # Get class predictions to calculate accuracy and mean confidence
    preds = [(pred.max(), pred.argmax()) for pred in predictions]
    preds_max, preds_pos = zip(*preds)
    if targets is not None:
        preds_corr = np.equal(targets, preds_pos)
        accuracy = np.sum(preds_corr) / len(preds_corr)
    confidence = np.mean(preds_max)

    # Calculate bin weights to norm
    weights = np.ones_like(preds_max) / float(len(preds_max))

    unified_bins = np.arange(0., 1. + bins**-1, bins**-1)

    # Plot max preds in bins
    plt.xlabel("Confidence ($\\tau$)")
    plt.ylabel("% of Samples")
    plt.ylim(0., 1.)
    plt.xlim(0., 1.)
    plt.hist(preds_max, color=color, bins=unified_bins, weights=weights)
    plt.axvline(confidence, color=color, linestyle='dashed', linewidth=1)
    legend_text = ["Avg. Confidence ({:.4f})".format(confidence)]

    if targets is not None:
        plt.axvline(accuracy, color='#d62728', linestyle='dashed', linewidth=1)
        legend_text.append("Accuracy ({:.4f})".format(accuracy))
    plt.legend(legend_text, fontsize="small")

    if save_fig is not None:
        plt.savefig(save_fig, bbox_inches="tight")

    if title is not None:
        plt.title(title)

    plt.show()


def cumulative_confidence_curve(predictions, labels, title=None, bins=100, save_fig=None):
    # Calculate unified bins for all histograms
    unified_bins = np.arange(0., 1. + bins**-1, bins**-1)

    fig = plt.figure(figsize=(6, 4))
    fig.tight_layout()

    for i, pred in enumerate(predictions):
        num_examples, confidence = np.histogram(np.max(pred, axis=1), bins=unified_bins)
        cumulative_examples = np.cumsum(num_examples)
        # Plot surival cumulative curve
        plt.plot(confidence[:-1], len(pred) - cumulative_examples)

    plt.xlabel("Confidence ($\\tau$)")
    # plt.ylabel("# of Samples $p(y|x) \\geq \\tau$")
    plt.legend(labels, fontsize="small")

    if title is not None:
        plt.title(title)

    if save_fig is not None:
        plt.savefig(save_fig, bbox_inches="tight")


# ------------------------------------------------------------
# --------------- SPIRAL SCRIPT PLOTS ------------------------
# ------------------------------------------------------------
cmap = plt.cm.copper  # colormap for plots


def plot_spiral_data(X, targets, out_of_train, mark_point=None, filename=None, title=None):
    """ python function to plot the data
    """
    coarse = np.arange(-1., 1.1, .5)
    fine = np.arange(-1, 1.01, .01)

    x_grid = np.meshgrid(coarse, fine)
    y_grid = np.meshgrid(fine, coarse)

    # grid_points_x = np.concatenate((x_grid[0].reshape(-1, 1), x_grid[1].reshape(-1, 1)), axis=1)
    # grid_points_y = np.concatenate((y_grid[0].reshape(-1, 1), y_grid[1].reshape(-1, 1)), axis=1)
    # grid_points = np.concatenate((grid_points_x, grid_points_y), axis=0)

    plt.figure(figsize=(8, 8))
    plt.scatter(X[:, 0], X[:, 1], c=targets, s=40, cmap=cmap)
    plt.scatter(x_grid[0].reshape(-1), x_grid[1].reshape(-1), marker='.', color='k', lw=0., s=2)
    plt.scatter(y_grid[0].reshape(-1), y_grid[1].reshape(-1), marker='.', color='k', lw=0., s=2)
    plt.xlabel("$x_1$")
    plt.ylabel("$x_2$")

    plt.scatter(out_of_train[:, 0], out_of_train[:, 1], marker="x", color="b", s=100)
    if mark_point is not None:
        plt.scatter(mark_point[0], mark_point[1], marker=".", color="g", s=100)

    if title:
        plt.title(title)

    if filename:
        plt.savefig(filename)


def _plot_decision_boundaries(forward_probability, X, targets, X_test,
                              x_lim=1.1, y_lim=1.1, plot_label=True,
                              title=None, resolution=40, marker_size=40,
                              mark_point=None
                              ):
    """ Plot function for the predictions in input data space
    """
    plt.xlim(-x_lim, x_lim)
    plt.ylim(-y_lim, y_lim)

    delta = x_lim / resolution
    a = np.arange(-x_lim, x_lim + delta, delta)
    b = np.arange(-y_lim, y_lim + delta, delta)
    A, B = np.meshgrid(a, b)

    x_ = np.dstack((A, B)).reshape(-1, 2)
    out = forward_probability(x_)

    ns = list()
    ns.append(len(np.unique(targets)))  # nb_of_different classes
    ns.extend(A.shape)
    out = out.T.reshape(ns)

    plt.pcolor(A, B, out[0], cmap="Oranges", alpha=0.2)
    plt.pcolor(A, B, out[1], cmap="Blues", alpha=0.2)
    plt.pcolor(A, B, out[2], cmap="Greens", alpha=0.2)

    # visualize the data:
    plt.scatter(X[:, 0], X[:, 1], c=targets, s=marker_size, cmap=cmap)

    if mark_point is not None:
        plt.scatter(mark_point[0], mark_point[1], s=marker_size, facecolors='none', edgecolors='r')

    if plot_label:
        plt.xlabel("$x_0$")
        plt.ylabel("$x_1$")

    if X_test is not None:
        plt.scatter(X_test[:, 0], X_test[:, 1], marker="+", color="b", s=100)


def plot_decision_boundaries(forward_probability, X, targets, X_test=None,
                             filename=None, x_lim=1.1, y_lim=1.1, fig=None,
                             plot_label=True, title=None, mark_point=None
                             ):
    """ Wrapper for plot function for the predictions in input data space
    """
    if fig is None:
        fig = plt.figure(figsize=(8, 8))

    fig.tight_layout()

    _plot_decision_boundaries(forward_probability, X, targets, X_test=X_test, x_lim=x_lim, y_lim=y_lim, plot_label=plot_label, title=title, mark_point=mark_point)

    if title:
        plt.title(title)

    if filename:
        plt.savefig(filename)

    plt.show()


def _plot_linear_decision_boundaries(model, W4, b4, fig, x_max, title, delta):
    """ plot function of the (training) data and predictions in
        the last hidden layer to successful seperate the classes
        with a softmax layer the decision boundaries must be linear
    """
    x_min = -.1
    plt.xlim(x_min, x_max)
    plt.ylim(x_min, x_max)

    a = np.arange(x_min, x_max + delta, delta)
    b = np.arange(x_min, x_max + delta, delta)
    A, B = np.meshgrid(a, b)
    x_ = np.dstack((A, B)).reshape(-1, 2)

    ns = list()
    ns.append(3)
    ns.extend(A.shape)

    out = x_.dot(W4) + b4

    out = out.T.argmax(axis=0).reshape(-1, 1)
    ohe = sklearn.preprocessing.OneHotEncoder(categories='auto')
    out = ohe.fit_transform(out).toarray()
    out = out.T.reshape(ns)

    plt.pcolor(A, B, out[0], cmap="Oranges", alpha=0.15)
    plt.pcolor(A, B, out[1], cmap="Blues", alpha=0.2)
    plt.pcolor(A, B, out[2], cmap="Greens", alpha=0.2)

    plt.xlabel("$h^{(l)}_0$")
    plt.ylabel("$h^{(l)}_1$")

    if title:
        plt.title(title)

# Wrapper for plot function of the (training) data and predictions in the last hidden layer
def plot_linear_decision_boundaries(model, X, targets, W4, b4, X_test=None, fig=None,  filename=None, x_max=45.1, title=None, delta=0.3, mark_point=None):

    if not fig:
        fig = plt.figure(figsize=(8, 8))

    _plot_linear_decision_boundaries(model, W4, b4, fig, x_max, title, delta)

    # Plot the training data
    if X is not None and targets is not None:
        h_ = model.last_hidden(Node(X)).value
        plt.scatter(h_[:, 0], h_[:, 1], c=targets, s=40, cmap=cmap)
        if mark_point is not None:
            plt.scatter(h_[mark_point][0], h_[mark_point][1], s=40, facecolors='none', edgecolors='r')

    if X_test is not None:
        h_test = model.last_hidden(Node(X_test)).value
        plt.scatter(h_test[:, 0], h_test[:, 1], marker="+", color="b", s=100)

    if filename is not None:
        plt.savefig(filename)


##########################################################################
##########################################################################
##########################################################################


def plot_entropy_hist(predictions, title, bins=1000, save_fig=None, close=False):
    """ Creates a Cumulative Distribution Function of Entropies for given predictions
    Args:
    predictions: List of numpy arrays
        Each array represents a prediction of a neural network of the same dataset
    title: List of strings
        Plot title for the corresponding prediction. Should be the lenght of 'predictions'
    save_fig: String - default: None
        If a path is given, the plot will be saved in that location.
    """
    # TODO:
    # Add assert for len(title) == len(predictions)
    # Add assert class_num is equal

    # Calculate entropies   
    num_classes = predictions[0].shape[1]
    entropies = []
    for prediction in predictions:
        entropies.append([scipy.stats.entropy(prediction[i, :]) for i in range(prediction.shape[0])])

    # Create plot
    plt.figure(figsize=(8, 5))
    x_axis = np.linspace(0, np.log(num_classes), bins)

    for i, entropy_rpl in enumerate(entropies):
        cdf = np.histogram(entropy_rpl, bins=bins)[0]
        # cdf = cdf / np.sum(cdf)
        # cdf = np.cumsum(cdf)
        plt.plot(x_axis, cdf, label="{}".format(title[i]))

    plt.legend()
    plt.xlim(0, np.log(num_classes) + 0.03)
    plt.xlabel("Entropy")
    plt.ylabel("# of Samples")
    if save_fig is not None:
        plt.savefig(save_fig, bbox_inches="tight")
    if close is True:
        plt.close()


def plot_roc(predictions, targets, avg_only=False, title="ROC", verbose=False, save_fig=None):
    """ Creates a receiver operating characteristic plot for multiclass predictions

    Compute ROC curve and ROC area for each class - heavily based on https://scikit-learn.org/stable/auto_examples/model_selection/plot_roc.html

    Args:
    predictions: Numpy array
        Array represents the predictions of a neural network
    targets: Numpy array
        Ground truth in vector representation
    avg_only: Bool - default: False

    verbose: Bool - default: False
        If 'True' in addition the confusion matrix for the prediction is printed
    save_fig: String - default: None
        If a path is given, the plot will be saved in that location.
    """
    fpr = dict()
    tpr = dict()
    roc_auc = dict()

    # Extract number of classes
    num_classes = predictions.shape[1]

    # Print the corresponding confusion matrix
    if verbose:
        conf_matrix = confusion_matrix(targets, np.argmax(predictions, axis=1))
        print(conf_matrix)

    # Compute ROC curves and AUCs
    for i in range(num_classes):
        fpr[i], tpr[i], _ = roc_curve(targets, predictions[:, i], pos_label=i)
        roc_auc[i] = auc(fpr[i], tpr[i])

    # Add micro-average
    fpr["micro"], tpr["micro"], _ = roc_curve(np.eye(num_classes)[targets].ravel(), predictions.ravel())
    roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])

    # Add macro-average:
    #  Aggregate all false positive rates -> interpolate all ROC curves at this points -> average everything and compute AUC
    all_fpr = np.unique(np.concatenate([fpr[i] for i in range(num_classes)]))
    mean_tpr = np.zeros_like(all_fpr)
    for i in range(num_classes):
        mean_tpr += interp(all_fpr, fpr[i], tpr[i])
    mean_tpr /= num_classes
    fpr["macro"] = all_fpr
    tpr["macro"] = mean_tpr
    roc_auc["macro"] = auc(fpr["macro"], tpr["macro"])

    # Plot all ROC curves
    plt.figure(figsize=[6, 6])

    plt.plot(fpr["micro"], tpr["micro"], label='Micro Average (Area = {0:0.2f})'.format(roc_auc["micro"]), linestyle=':', linewidth=3)
    plt.plot(fpr["macro"], tpr["macro"], label='Macro Average (Area = {0:0.2f})'.format(roc_auc["macro"]), linestyle=':', linewidth=3)

    if not avg_only:
        for i in range(num_classes):
            plt.plot(fpr[i], tpr[i], label='Class {0} (Area = {1:0.2f})'.format(i, roc_auc[i]))

    plt.plot([0, 1], [0, 1], 'k--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title(title)
    plt.legend(loc="lower right")

    if save_fig is not None:
        plt.savefig(save_fig, bbox_inches="tight")


def plot_roc_curve(outputs, targets, model_name, fig_path, log_file=None, close=False):

    # Plot ROC curves
    if 'rpl' in model_name:
        plot_roc(outputs,
                 targets,
                 avg_only=True, title="RPL - Receiver Operating Characteristic",
                 verbose=True,
                 save_fig=os.path.join(fig_path, "{}_ROC-AUC_RPL.png".format(model_name)),
                 log_file=log_file,
                 close=close
                 )
    else:
        plot_roc(outputs,
                 targets,
                 avg_only=True,
                 title="Softmax - Receiver Operating Characteristic",
                 verbose=True,
                 save_fig=os.path.join(fig_path, "{}_ROC-AUC_Softmax.png".format(model_name)),
                 log_file=log_file,
                 close=close
                 )


def ece_mce_calc(predictions, targets, bins=10, verbose=False):
    # Define bins in interval [0,1]
    bins = np.arange(0, 1, 1 / bins)

    # Get maximum prediction and corresponding class
    preds = [(pred.max(), pred.argmax()) for pred in predictions]
    preds_max, preds_pos = zip(*preds)

    # Sort predictions into bins
    preds_bins = np.digitize(preds_max, bins) - 1

    # Check which prediction is correct and calculate accuracy
    preds_corr = np.equal(targets, preds_pos)
    # accuracy = np.sum(preds_corr)/len(preds_corr)

    # Calculate accuracy and confidence for each bin
    bin_acc = [[] for bin in bins]
    bin_conf = [[] for bin in bins]
    for i, bin in enumerate(preds_bins):
        bin_conf[bin].append(preds_max[i])
        bin_acc[bin].append(preds_corr[i])
    bin_cardinality = np.asarray([len(bin) if bin else 0 for bin in bin_acc])
    bin_acc = np.asarray([np.sum(bin) / len(bin) if bin else 0 for bin in bin_acc])
    bin_conf = np.asarray([np.sum(bin) / len(bin) if bin else 0 for bin in bin_conf])

    # Calculate ECE and MCE
    abs_diff = np.abs(bin_acc - bin_conf)
    bin_weight = bin_cardinality / len(preds_max)
    ece = np.sum(bin_weight * abs_diff)
    mce = np.max(abs_diff)

    if verbose:
        return ece, mce, bin_acc, bin_cardinality, bins
    return ece, mce


def reliability_diagram(predictions, targets, bins=10, title=None, save_fig=None):
    """
    """
    ece, mce, bin_acc, bin_cardinality, bins = ece_mce_calc(predictions, targets, bins, verbose=True)

    # Calculate optimal bin accuracies
    bin_diff = bins[1] / 2
    bin_acc_perfect = np.asarray([bin + bin_diff for bin in bins])
    no_elements = bin_cardinality == 0.
    bin_acc_perfect[no_elements] = 0.

    # Plotting
    if title is not None:
        plt.title(title)
    plt.xlabel("Confidence")
    plt.ylabel("Accuracy")
    plt.xlim(0., 1.)
    plt.ylim(0., 1.)
    plt.plot([0, 1], color='r')
    plt.bar(bins, bin_acc_perfect, align='edge', color='lightcoral', edgecolor='black', hatch="/", alpha=0.95, width=1 / len(bins))
    plt.bar(bins, bin_acc, align='edge', color='blue', edgecolor='black', width=1 / len(bins))
    plt.text(0.05, 0.95, "ECE: {:.4f}\nMCE: {:.4f}".format(ece, mce),
             fontsize=14,
             verticalalignment='top',
             bbox=dict(boxstyle='square',
                       facecolor='white',
                       alpha=0.8)
             )
    plt.grid()
    if save_fig is not None:
        plt.savefig(save_fig, bbox_inches="tight")
    plt.show()
