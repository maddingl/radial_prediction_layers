import torch
import torch.nn as nn
import torch.nn.functional as F

import torchvision.models as models

from rpl.rpl_pt import RadialPredictionLayer


class LeNet(nn.Module):
    """
    """

    def __init__(self, num_classes=10, dropout=0.5, use_rpl=False, alpha=1.0):
        super(LeNet, self).__init__()
        self.use_rpl = use_rpl
        self.dropout = dropout
        self.rpl = RadialPredictionLayer(num_classes, num_classes, prototype_fixed=True, alpha=alpha)
        self.features = nn.Sequential(
            nn.Conv2d(1, 10, kernel_size=5),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(2),
            nn.Conv2d(10, 20, kernel_size=5),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(2),
        )
        self.classifier = nn.Sequential(
            nn.Dropout(p=self.dropout),
            nn.Linear(320, 100),
            nn.ReLU(inplace=True),
            nn.Dropout(p=self.dropout),
            nn.Linear(100, 100),
            nn.ReLU(inplace=True),
            nn.Linear(100, num_classes),
        )

    def forward(self, x, temperature=1.0):
        x = self.features(x)
        x = torch.flatten(x, 1)
        x = self.classifier(x)
        if self.use_rpl:
            return self.rpl(x)
        x = x / temperature
        return F.log_softmax(x, dim=1)

    def probabilities(self, x, beta=1.0, temperature=1.0):
        out = self.forward(x, temperature)
        return torch.exp(beta * out)


class AlexNet(nn.Module):
    """
    """

    def __init__(self, num_classes=10, pretrained=True, freeze_pretrained=True, use_rpl=False, dropout=0.0, alpha=1.0):
        super(AlexNet, self).__init__()
        self.use_rpl = use_rpl
        self.dropout = dropout
        base_model = models.alexnet(pretrained=pretrained)

        # Freeze pretrained params
        if freeze_pretrained is True:
            for param in base_model.parameters():
                param.requires_grad = False

        # Get last layer input dim and modify last layer
        feature_out_dim = base_model.classifier[1].in_features

        # Take feature layer from the pretrained
        self.features = nn.Sequential(*list(base_model.children())[:-1])

        # Take feature layer from the pretrained
        self.features = nn.Sequential(*list(base_model.children())[:-1])

        # Initialize new classifier layer
        self.classifier = nn.Sequential(nn.Linear(feature_out_dim, feature_out_dim),
                                        nn.ReLU(),
                                        nn.Linear(feature_out_dim, feature_out_dim),
                                        nn.ReLU(),
                                        nn.Linear(feature_out_dim, num_classes))

        # Initialize predictor: RPL or Softmax
        if self.use_rpl:
            self.predictor = RadialPredictionLayer(num_classes, num_classes, prototype_fixed=True, alpha=alpha)
        else:
            self.predictor = nn.LogSoftmax(dim=1)

    def forward(self, x):
        x = self.features(x)
        x = torch.flatten(x, 1)
        x = self.classifier(x)
        return self.predictor(x)

    def predict(self, x, beta=1.0):
        out = self.forward(x)
        return torch.exp(beta * out)


class VGG19(nn.Module):
    """
    """

    def __init__(self, num_classes=10, pretrained=True, freeze_pretrained=True, use_rpl=False, dropout=0.0, alpha=1.0, linear=4096):
        super(VGG19, self).__init__()
        self.use_rpl = use_rpl
        self.dropout = dropout
        base_model = models.vgg19_bn(pretrained=pretrained)

        # Freeze pretrained params
        if freeze_pretrained is True:
            for param in base_model.parameters():
                param.requires_grad = False

        # Get last layer input dim and modify last layer
        feature_out_dim = base_model.classifier[0].in_features

        # Take feature layer from the pretrained
        self.features = nn.Sequential(*list(base_model.children())[:-1])

        # Initialize new classifier layer
        self.classifier = nn.Sequential(nn.Linear(feature_out_dim, linear),
                                        nn.ReLU(),
                                        nn.Dropout(p=self.dropout),
                                        nn.Linear(linear, 1000),
                                        nn.ReLU(),
                                        nn.Linear(1000, num_classes))

        # Initialize predictor: RPL or Softmax
        if self.use_rpl:
            self.predictor = RadialPredictionLayer(num_classes, num_classes, prototype_fixed=True, alpha=alpha)
        else:
            self.predictor = nn.LogSoftmax(dim=1)

    def forward(self, x):
        x = self.features(x)
        x = torch.flatten(x, 1)
        x = self.classifier(x)
        return self.predictor(x)

    def predict(self, x, beta=1.0):
        out = self.forward(x)
        return torch.exp(beta * out)


class ResNet50(nn.Module):
    """
    """

    def __init__(self, num_classes=10, pretrained=True, freeze_pretrained=True, use_rpl=False, dropout=0.0, alpha=1.0):
        super(ResNet50, self).__init__()
        self.use_rpl = use_rpl
        self.dropout = dropout
        base_model = models.resnet50(pretrained=pretrained)

        # Freeze pretrained params
        if freeze_pretrained is True:
            for param in base_model.parameters():
                param.requires_grad = False

        # Get last layer input dim and modify last layer
        feature_out_dim = base_model.fc.in_features

        # Take feature layer from the pretrained
        self.features = nn.Sequential(*list(base_model.children())[:-1])

        # Initialize new classifier layer
        self.classifier = nn.Sequential(nn.Linear(feature_out_dim, num_classes))

        # Initialize predictor: RPL or Softmax
        if self.use_rpl:
            self.predictor = RadialPredictionLayer(num_classes, num_classes, prototype_fixed=True, alpha=alpha)
        else:
            self.predictor = nn.LogSoftmax(dim=1)

    def forward(self, x):
        x = self.features(x)
        x = torch.flatten(x, 1)
        x = self.classifier(x)
        return self.predictor(x)

    def predict(self, x, beta=1.0):
        out = self.forward(x)
        return torch.exp(beta * out)
